from bonus_system import calculateBonuses


def test_standart():
    assert calculateBonuses("Standard", 0) == 0.5 * 1
    assert calculateBonuses("Standard", 1) == 0.5 * 1
    assert calculateBonuses("Standard", 9999) == 0.5 * 1
    assert calculateBonuses("Standard", 10000) == 0.5 * 1.5
    assert calculateBonuses("Standard", 10001) == 0.5 * 1.5
    assert calculateBonuses("Standard", 49999) == 0.5 * 1.5
    assert calculateBonuses("Standard", 50000) == 0.5 * 2
    assert calculateBonuses("Standard", 50001) == 0.5 * 2
    assert calculateBonuses("Standard", 99999) == 0.5 * 2
    assert calculateBonuses("Standard", 100000) == 0.5 * 2.5
    assert calculateBonuses("Standard", 100001) == 0.5 * 2.5

def test_premium():
    assert calculateBonuses("Premium", 0) == 0.1 * 1
    assert calculateBonuses("Premium", 1) == 0.1 * 1
    assert calculateBonuses("Premium", 9999) == 0.1 * 1
    assert calculateBonuses("Premium", 10000) == 0.1 * 1.5
    assert calculateBonuses("Premium", 10001) == 0.1 * 1.5
    assert calculateBonuses("Premium", 49999) == 0.1 * 1.5
    assert calculateBonuses("Premium", 50000) == 0.1 * 2
    assert calculateBonuses("Premium", 50001) == 0.1 * 2
    assert calculateBonuses("Premium", 99999) == 0.1 * 2
    assert calculateBonuses("Premium", 100000) == 0.1 * 2.5
    assert calculateBonuses("Premium", 100001) == 0.1 * 2.5

def test_diamond():
    assert calculateBonuses("Diamond", 0) == 0.2 * 1
    assert calculateBonuses("Diamond", 1) == 0.2 * 1
    assert calculateBonuses("Diamond", 9999) == 0.2 * 1
    assert calculateBonuses("Diamond", 10000) == 0.2 * 1.5
    assert calculateBonuses("Diamond", 10001) == 0.2 * 1.5
    assert calculateBonuses("Diamond", 49999) == 0.2 * 1.5
    assert calculateBonuses("Diamond", 50000) == 0.2 * 2
    assert calculateBonuses("Diamond", 50001) == 0.2 * 2
    assert calculateBonuses("Diamond", 99999) == 0.2 * 2
    assert calculateBonuses("Diamond", 100000) == 0.2 * 2.5
    assert calculateBonuses("Diamond", 100001) == 0.2 * 2.5

def test_incorrect():
    assert calculateBonuses("", 1) == 0
    assert calculateBonuses("standart", 1) == 0
    assert calculateBonuses("diamondd", 1) == 0
    assert calculateBonuses("Pemium", 1) == 0
